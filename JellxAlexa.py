#!/usr/bin/env python

import email.utils
import requests
import select
import socket
import struct
import sys
import time
import urllib
import uuid
import json
import fcntl

# TCP Port for server
TCPPort = 80 #Port 80 is needed for Alexa Gen3
#Jellx Device Name
Device = "JELLX"

DEVICES = {}


WLAN = 'wlp5s0'

# Base URL for API calls to Nodejs
BaseURL = "https://rthtmypa.p73.rt3.io"
BaseURL2 = ""
#BaseURL = "http://fbfsmpxe.p71.rt3.io"

SETUP_XML = """<?xml version="1.0" ?>
<root xmlns="urn:schemas-upnp-org:device-1-0">
    <specVersion><major>1</major><minor>0</minor></specVersion>
    <URLBase>http://%s:%d/</URLBase>
    <device>
        <deviceType>urn:schemas-upnp-org:device:Basic:1</deviceType>
        <friendlyName>Philips hue  %s:%d </friendlyName>
        <manufacturer>Royal Philips Electronics</manufacturer>
        <manufacturerURL>http://www.philips.com</manufacturerURL>
        <modelDescription>Philips hue Personal Wireless Lighting</modelDescription>
        <modelName>Philips hue bridge 2012</modelName>
        <modelNumber>929000226503</modelNumber>
        <modelURL>http://www.meethue.com</modelURL>
        <serialNumber>%s</serialNumber>
        <UDN>uuid:2f402f80-da50-11e1-9b23-%s</UDN>
        <presentationURL>index.html</presentationURL>
    </device>
</root>
"""
#This is get state event response data
# to the Amazon Echo

DEBUG = False

def dbg(msg):
    global DEBUG
    if DEBUG:
        print msg
        sys.stdout.flush()


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

def getMAC(interface=WLAN):
  # Return the MAC address of the specified interface
  try:
    str = open('/sys/class/net/%s/address' %interface).read()
  except:
    str = "000000000000"
  return str[0:17].replace(":" , "")




# A simple utility class to wait for incoming data to be
# ready on a socket.

class poller:
    def __init__(self):
        if 'poll' in dir(select):
            self.use_poll = True
            self.poller = select.poll()
        else:
            self.use_poll = False
        self.targets = {}

    def add(self, target, fileno = None):
        if not fileno:
            fileno = target.fileno()
        if self.use_poll:
            self.poller.register(fileno, select.POLLIN)
        self.targets[fileno] = target

    def remove(self, target, fileno = None):
        if not fileno:
            fileno = target.fileno()
        if self.use_poll:
            self.poller.unregister(fileno)
        del(self.targets[fileno])

    def poll(self, timeout = 0):
        if self.use_poll:
            ready = self.poller.poll(timeout)
        else:
            ready = []
            if len(self.targets) > 0:
                (rlist, wlist, xlist) = select.select(self.targets.keys(), [], [], timeout)
                ready = [(x, None) for x in rlist]
        for one_ready in ready:
            target = self.targets.get(one_ready[0], None)
            if target:
                target.do_read(one_ready[0])


# Base class for a generic UPnP device. This is far from complete
# but it supports either specified or automatic IP address and port
# selection.

class upnp_device(object):
    this_host_ip = None

    @staticmethod
    def local_ip_address():
        if not upnp_device.this_host_ip:
            upnp_device.this_host_ip = get_ip_address(WLAN)
            dbg("got local address of %s" % upnp_device.this_host_ip)
        return upnp_device.this_host_ip


    def __init__(self, listener, poller, port, root_url, server_version, persistent_uuid, other_headers = None, ip_address = None):
        self.listener = listener
        self.poller = poller
        self.port = port
        self.root_url = root_url
        self.server_version = server_version
        self.persistent_uuid = persistent_uuid
        self.uuid = uuid.uuid4()
        self.other_headers = other_headers

        if ip_address:
            self.ip_address = ip_address
        else:
            self.ip_address = upnp_device.local_ip_address()

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.ip_address, self.port))
        self.socket.listen(5)
        if self.port == 0:
            self.port = self.socket.getsockname()[1]
        self.poller.add(self)
        self.client_sockets = {}
        self.listener.add_device(self)

    def fileno(self):
        return self.socket.fileno()

    def do_read(self, fileno):
        if fileno == self.socket.fileno():
            (client_socket, client_address) = self.socket.accept()
            self.poller.add(self, client_socket.fileno())
            self.client_sockets[client_socket.fileno()] = client_socket
        else:
            data, sender = self.client_sockets[fileno].recvfrom(4096)
            if not data:
                self.poller.remove(self, fileno)
                del(self.client_sockets[fileno])
            else:
                self.handle_request(data, sender, self.client_sockets[fileno])

    def handle_request(self, data, sender, socket):
        pass

    def get_name(self):
        return "unknown"


    def respond_to_search(self, destination, search_target):
        dbg("Responding to search for %s" % self.get_name())
        date_str = email.utils.formatdate(timeval=None, localtime=False, usegmt=True)
        location_url = self.root_url % {'ip_address' : self.ip_address, 'port' : self.port}
        mac = getMAC()
        message = ("HTTP/1.1 200 OK\r\n"
                  "CACHE-CONTROL: max-age=100\r\n"
                  "DATE: %s\r\n"
                  "EXT:\r\n"
                  "LOCATION: %s\r\n"
                  "OPT: \"http://schemas.upnp.org/upnp/1/0/\"; ns=01\r\n"
                  "SERVER: FreeRTOS/6.0.5, UPnP/1.0, IpBridge/1.17.0\r\n"
                  "hue-bridgeid: %s\r\n"
                  "ST: urn:schemas-upnp-org:device:basic:1\r\n"
                  "USN: uuid:2f402f80-da50-11e1-9b23-%s::upnp:rootdevice\r\n" % (date_str, location_url , mac , mac))
        message += "\r\n"
	dbg(message)
        temp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        temp_socket.sendto(message, destination)


# This subclass does the bulk of the work to mimic a WeMo switch on the network.

def SendHTTPResponse(socket , Code , Body , ContentType):
    TCP_HEADERS =("HTTP/1.1 %s\r\n"
    "Content-Type: %s\r\n"
    "Content-Length: %d\r\n"
    "Connection: close\r\n\r\n"
    "%s")
    data = TCP_HEADERS % ( Code , ContentType , len(Body) , Body )
    socket.send(data)

def CreateDevice(Name ,  Port):
        d = dict([
                ('type' , 'Extended Color Light'),
                ('name' , Name ),
                ('uniqueid' , Name + '-' + str(Port)),
                ('modelid' , 'LCT007'),
                ('state' , dict([
                                ('on' , True),
                                ('bri' , 255),
                                ('xy' , [0 , 0]),
                                ('reachable' , True)
                        ])),
                ('capabilities' , dict([
                                ('certified' ,  False),
                                ('streaming' , dict([
                                                ('rendered' , True),
                                                ('proxy' , False)
                                                ]))
                                ])),
                ('swversion' , '5.105.0.21169')
        ])
        DEVICES[Port] = d



class fauxmo(upnp_device):
    @staticmethod
    def make_uuid(name):
        return ''.join(["%x" % sum([ord(c) for c in name])] + ["%x" % ord(c) for c in "%sfauxmo!" % name])[:14]

    def __init__(self, name, listener, poller, ip_address, port, action_handler = None):
        self.serial = self.make_uuid(name)
        self.name = name
	self.port = port
        self.ip_address = ip_address
        persistent_uuid = "Socket-1_0-" + self.serial
        other_headers = ['X-User-Agent: redsonic']
        upnp_device.__init__(self, listener, poller, port, "http://%(ip_address)s:%(port)s/setup.xml", "Unspecified, UPnP/1.0, Unspecified", persistent_uuid, other_headers=other_headers, ip_address=ip_address)
        if action_handler:
            self.action_handler = action_handler
        else:
            self.action_handler = self
        dbg("FauxMo device '%s' ready on %s:%s" % (self.name, self.ip_address, self.port))

    def get_name(self):
        return self.name

    def handle_request(self, data, sender, socket):
        if data.find('GET /setup.xml HTTP/1.1') == 0:
            dbg("Responding to setup.xml for %s" % self.name)
            ip = get_ip_address(WLAN)
            mac = getMAC()
            xml = SETUP_XML % (ip , TCPPort , ip , TCPPort , str(self.serial) , mac )
            print xml
            SendHTTPResponse(socket , '200 OK' , xml , ' text/xml')
	elif data.find('GET /api') != -1:
            arr = data.split('/')
            if arr[3] == 'lights HTTP':
              print DEVICES
              SendHTTPResponse(socket , '200 OK' , json.dumps(DEVICES) , 'application/json')
              dbg("Sending List of Devices")
            elif arr[3]  == 'lights' and  arr[4].find('HTTP') != -1:
              arr1 = arr[4].split(' ')
              State = self.action_handler.state(int(arr1[0]))
              DEVICES['lights'][int(arr1[0])]['state']['on'] = True if State else False
              SendHTTPResponse(socket , '200 OK' , json.dumps(DEVICES['lights'][int(arr1[0])]) , 'application/json')
              dbg("Sending State for Light %s" % arr1[0])
            else:
              dbg("Unknown Command")
        elif data.find('POST /api') != -1:
              dbg("Responding with all Information\r\n\r\n\r\n")
              SendHTTPResponse(socket , '200 OK' ,"[{\"success\":{\"username\":\"e7x4kuCaC8h885jo\"}}]", 'application/json')
        elif data.find('PUT /api') != -1:
              #PUT /api/yNn9n1MQhDYZuPeUWcWn3YwKlU1xR8YZ0Feo4jWn/lights/2/state HTTP/1.1
              Response = """[{"success":{"/lights/%d/state/on":%s}}]"""
              arr = data.split('/')
              if arr[3] == 'lights' and arr[4]:
                d = json.loads( data[data.find('\r\n\r\n'):])
                if d['on'] == True :
                  if self.action_handler.on(int(arr[4])) == 1:
                    DEVICES['lights'][int(arr[4])]['state']['on'] = True
                    SendHTTPResponse(socket , '200 OK' , Response % ( int(arr[4]) , "true") , 'application/json')
                elif d['on'] == False:
                  if self.action_handler.off(int(arr[4])) == 1:
                    DEVICES['lights'][int(arr[4])]['state']['on'] = False
                    SendHTTPResponse(socket , '200 OK' , Response % (int(arr[4]) , "false") , 'application/json')
                dbg('Control API')
        else:
            dbg(data)

# Since we have a single process managing several virtual UPnP devices,
# we only need a single listener for UPnP broadcasts. When a matching
# search is received, it causes each device instance to respond.
#
# Note that this is currently hard-coded to recognize only the search
# from the Amazon Echo for WeMo devices. In particular, it does not
# support the more common root device general search. The Echo
# doesn't search for root devices.

class upnp_broadcast_responder(object):
    TIMEOUT = 0

    def __init__(self):
        self.devices = []

    def init_socket(self):
        ok = True
        self.ip = '239.255.255.250'
        self.port = 1900
        try:
            #This is needed to join a multicast group
            self.mreq = struct.pack("4sl",socket.inet_aton(self.ip),socket.INADDR_ANY)

            #Set up server socket
            self.ssock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM,socket.IPPROTO_UDP)
            self.ssock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)

            try:
                self.ssock.bind(('',self.port))
            except Exception, e:
                dbg("WARNING: Failed to bind %s:%d: %s" , (self.ip,self.port,e))
                ok = False

            try:
                self.ssock.setsockopt(socket.IPPROTO_IP,socket.IP_ADD_MEMBERSHIP,self.mreq)
            except Exception, e:
                dbg('WARNING: Failed to join multicast group:',e)
                ok = False

        except Exception, e:
            dbg("Failed to initialize UPnP sockets:",e)
            return False
        if ok:
            dbg("Listening for UPnP broadcasts")

    def fileno(self):
        return self.ssock.fileno()

    def do_read(self, fileno):
        data, sender = self.recvfrom(1024)
        if data:
            if data.find('M-SEARCH') == 0 and (data.find('upnp:rootdevice') > 0 or data.find('device:basic:1')):
                dbg(data)
                for device in self.devices:
                    time.sleep(0.1)
                    device.respond_to_search(sender, 'urn:Belkin:device:**')
            else:
                pass

    #Receive network data
    def recvfrom(self,size):
        if self.TIMEOUT:
            self.ssock.setblocking(0)
            ready = select.select([self.ssock], [], [], self.TIMEOUT)[0]
        else:
            self.ssock.setblocking(1)
            ready = True

        try:
            if ready:
                return self.ssock.recvfrom(size)
            else:
                return False, False
        except Exception, e:
            dbg(e)
            return False, False

    def add_device(self, device):
        self.devices.append(device)
        dbg("UPnP broadcast listener: new device registered")


# This is an handler class. The fauxmo class expects handlers to be
# instances of objects that have on() and off() methods that return 1
# on success and 0 otherwise.

class JellXhandler(object):
	def on(self , port):
        #return has to be 1 for sucess and 0 for failure
		dbg("Turning ON %d " % port)
		data = {
			"alexa_port": port, 
			"is_on": True
			}

		try:
			r = requests.post(url = BaseURL + "/alexa/device/control", json = data)
		except Exception , e:
			dbg(str(e))
			return 0
		return 1

	def off(self, port):
		dbg("Turning OFF %d " % port)
		data = {
			"alexa_port": port, 
			"is_on": False
			}
		try:
			r = requests.post(url = BaseURL + "/alexa/device/control", json = data)
		except Exception , e:
			dbg(str(e))
			return 0
		return 1

	def state(self , port):
		State = 0
		dbg("Getting State for %d from DB" % port)
		try:
			r = requests.get(url = BaseURL + "/alexa/device/state?port=" + str(port))
			if json.loads(r.content)["is_on"] is True:
				State = 1
			elif json.loads(r.content)["is_on"] is False:
				State = 0
		except Exception , e:
			dbg(str(e))
		return State


# Each entry is a list with the following elements:
#
# name of the virtual switch
# port
if len(sys.argv) > 1 and sys.argv[1] == '-d':
    DEBUG = True
# Set up our singleton for polling the sockets for data ready
p = poller()
# Set up our singleton listener for UPnP broadcasts
u = upnp_broadcast_responder()
u.init_socket()

got = 0
while got is 0:
	try:
		r = requests.get(url = BaseURL + "/alexa/devices")
		output = json.loads(r.content)
		for n in range(0 , len(output['devices'])):
			CreateDevice(output['devices'][n][0] , output['devices'][n][1])
		if BaseURL2:
			r = requests.get(url = BaseURL + "/alexa/devices")
			output = json.loads(r.content)
			for n in range(0 , len(output['devices'])):
				CreateDevice(output['devices'][n][0] , output['devices'][n][1])
		got  = 1
	except Exception, e:
		dbg(e)

r = requests.get(url = BaseURL + "/alexa/devices")



print DEVICES

# Add the UPnP broadcast listener to the poller so we can respond
# when a broadcast is received.
p.add(u)
handler = JellXhandler()
switch = fauxmo( Device, u, p, None, TCPPort , action_handler = handler)



dbg("Entering main loop\n")

while True:
    try:
        # Allow time for a ctrl-c to stop the process
        p.poll(100)
        time.sleep(0.05)
    except Exception, e:
        dbg(e)
        break

